//
//  SettingsViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 28.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit
import UserNotifications


class SettingsViewController: UIViewController,DataEnteredDelegate,DateGenderDelegate, DateStatusDelegate,DateDataDelegate, DateTimeDelegate, UNUserNotificationCenterDelegate {
    func userDidEnteredInformation(zodiacImageProtocol: UIImage) {
        zodizImageOutlet.image = zodiacImageProtocol
    }
    
    
    func userDidEnteredInformation(zodiac: String) {
        zodiacLabelOutlet.text = zodiac
    }
    func userDidEnteredInformation(time: String) {
        timeUserSettings.text = time
        print("timeUser = time")
    }
    
    func userDidEnteredInformation( date: String) {
        dateUserSettings.text = date
        
        print("dateUser = date")
        
    }
    
    func userDidEnteredInformation(status: String) {
        
        statusUserSettings.text = status
        print("statusUser = status")
        
    }
    
    func userDidEnteredInformation(gender: String) {
        genderUserSettings.text = gender
        
        print("genderUser = gender")
    }
    
    func userDidEnterInformation(info: String) {
        nameUserSettings.text = info
        nameUserTitle.text = info
        Singleton.shared.nameUser = info
        
        
        print("nameUser = info")
    }
    
    
    @IBOutlet weak var nameUserSettings: UILabel!
    
    @IBOutlet weak var genderUserSettings: UILabel!
    @IBOutlet weak var switchPushOutlet: UISwitch!
    @IBOutlet weak var setButnDateOutlet: UIButton!
    @IBOutlet weak var statusUserSettings: UILabel!
    
    @IBOutlet weak var setButtnNameOutlet: UIButton!
    @IBOutlet weak var dateUserSettings: UILabel!
    
    @IBOutlet weak var timeUserSettings: UILabel!
    
    @IBOutlet weak var zodizImageOutlet: UIImageView!
    
    @IBOutlet weak var nameUserTitle: UILabel!
    
    @IBOutlet weak var zodiacLabelOutlet: UILabel!
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changeNameVCid" {
            let changeNameVc = segue.destination as! changeNameVC
            changeNameVc.delegate = self
            
            
        }
        if segue.identifier == "changeGenderVCid" {
            let changeGendereVc = segue.destination as! changeGenderVC
            changeGendereVc.delegate = self
            
            
        }
        if segue.identifier == "changeStatusVCid" {
            let changeStatusVc = segue.destination as! changeStatusVC
            changeStatusVc.delegate = self
            
            
        }
        if segue.identifier == "changeDateVCid"{
            let changeDateVc = segue.destination as! changeDateVC
            changeDateVc.delegate = self
            
            
        }
        if segue.identifier == "changeTimeVCid"{
            let changeTimeVc = segue.destination as! changeTimeVC
            changeTimeVc.delegate = self
            
            
        }
        
        
    }
    
    
    @IBAction func switchPushAction(_ sender: UISwitch) {
        print(" switch IF ")
        if(sender.isOn){
            print("on")
            PushViewController.push()
            defaults.set(sender.isOn, forKey: Key.switchUserSett)
            
        }  else{
            print("Off")
            UIApplication.shared.unregisterForRemoteNotifications()
            defaults.set(sender.isOn, forKey: Key.switchUserSett)
        }
        
    }
    
    
    func setLabel()
    {
        self.view.bringSubviewToFront(dateUserSettings)
        self.view.bringSubviewToFront(setButnDateOutlet)
        self.view.bringSubviewToFront(switchPushOutlet)
         self.view.bringSubviewToFront(nameUserSettings)
         self.view.bringSubviewToFront(setButtnNameOutlet)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad start ")
        
        
        setLabel()
        
        switchPushOutlet.tintColor = .red
        setupNavBar()
        
        getSettings()
        
    }
    
    //save user info
    
    let defaults = UserDefaults.standard
    
    enum Key {
        static let dateUser = "dateUser"
        static let timeUser = "timeUser"
        static let genderUser = "genderUser"
        static let statusUser = "statusUser"
        static let nameUser = "nameUser"
        static let zodiacImage = "settingsImageUser"
        static let nameTitleUser = "nameUser"
        static let zodiacUser = "zodiacUser"
        static let zodiacNameImageUser = "zodiacImageName"
        static let switchUserSett = "switchUser"
        
    }
    func getSettings() {
        nameUserTitle.text = Singleton.shared.nameUser
        nameUserSettings.text = Singleton.shared.nameUser
        nameUserSettings.text =  defaults.string(forKey: Key.nameUser)
        nameUserTitle.text = defaults.string(forKey: Key.nameTitleUser)
        
        genderUserSettings.text = Singleton.shared.genderUser
        genderUserSettings.text =  defaults.string(forKey: Key.genderUser)
        
        dateUserSettings.text = Singleton.shared.dateUser
        dateUserSettings.text =  defaults.string(forKey: Key.dateUser)
        
        timeUserSettings.text = Singleton.shared.timeUser
        timeUserSettings.text =  defaults.string(forKey: Key.timeUser)
        
        statusUserSettings.text = Singleton.shared.statusUser
        statusUserSettings.text =  defaults.string(forKey: Key.statusUser)
        
        zodiacLabelOutlet.text = Singleton.shared.zodiacUser
        zodiacLabelOutlet.text = defaults.string(forKey: Key.zodiacUser)
        
        let   textzodiac = defaults.string(forKey: Key.zodiacNameImageUser)!
        //  zodizImageOutlet.image = Singleton.shared.zodiacImage
        zodizImageOutlet.image = loadImageFromDiskWith(fileName:  textzodiac)
        
        
        switchPushOutlet.isOn = defaults.bool(forKey: Key.switchUserSett)
        
        print("Load Image \(Singleton.shared.nameImageZodiac)")
        
        
        print("Get Settings")
    }
    
    func loadImageFromDiskWith(fileName: String) -> UIImage? {
       
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            print("Load Image func \(Singleton.shared.nameImageZodiac)")
              return image
            
        }
        
        return nil
    }
    
    func setupNavBar()
    {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .white
        
        
    }
    override  func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}




