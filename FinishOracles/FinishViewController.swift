//
//  FinishViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 27.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit

class FinishViewController: UIViewController {
    
    @IBOutlet weak var TextLabelOutlet: UILabel!
    @IBOutlet weak var ProgressBarOutlet: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProgressBarOutlet.setProgress(0, animated: false)
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateProgress), userInfo: nil, repeats: true)
        
    }
    
    @objc func updateProgress() {
        
        
        if ProgressBarOutlet.progress != 1
        {
            self.ProgressBarOutlet.progress += 2 / 10
        }else{
            
            self.performSegue(withIdentifier: "viewNext", sender: self)
            
            
            
        }
        
    }
    
}
