//
//  changeDateVC.swift
//  Oracles
//
//  Created by Macbook Pro on 03.09.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//
//
//  DateViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 23.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit

//////


protocol DateDataDelegate: class {
    func userDidEnteredInformation(date: String)
    func userDidEnteredInformation(zodiac:String)
    func userDidEnteredInformation(zodiacImageProtocol: UIImage)
}

enum LINE_POSITION5 {
    case LINE_POSITION_TOP5
    case LINE_POSITION_BOTTOM5
}

extension UIView {
    func addLine(position : LINE_POSITION5, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP5:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM5:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}

/// CLASS


class changeDateVC: UIViewController ,UITextFieldDelegate,UIPickerViewDelegate{
    
    
    
    @IBOutlet weak var DateTextFieldOutlet: UITextField!
    

    @IBOutlet weak var changeButnDateOutlet: UIButton!
    @IBOutlet weak var titleTextChangeDateOutlet: UILabel!
    
    @IBOutlet weak var zodiacImage: UIImageView!
    
    weak var delegate: DateDataDelegate? = nil
    private let dateUser = "dateUser"
    private let zodiacNameUser = "zodiacUser"
    private let zodiacNameImage = "zodiacImageName"
    private let nameTitle = "nameUser"
    
    @IBAction func changeDate(_ sender: UIButton)
    {
        let information:String  =  DateTextFieldOutlet.text!
        delegate!.userDidEnteredInformation(date: information)
        
        let infoZodiac:String = Singleton.shared.zodiacUser
        delegate!.userDidEnteredInformation(zodiac: infoZodiac)
      
            let zodiacImageDelegate:UIImage = Singleton.shared.zodiacImage!
            delegate!.userDidEnteredInformation(zodiacImageProtocol: zodiacImageDelegate)
        
       
        
        self.navigationController?.popViewController(animated:true)

        print("Ready")
    }
    
    
    let defaults = UserDefaults.standard
    
    func saveDate()
    {
        defaults.set(Singleton.shared.dateUser, forKey: dateUser)
        defaults.set(DateTextFieldOutlet.text, forKey: dateUser)
        defaults.set(Singleton.shared.zodiacUser, forKey: zodiacNameUser)
        defaults.set(Singleton.shared.nameImageZodiac, forKey: zodiacNameImage)
         print("save finish")
    }
    
    func saveImage(imageName: String, image: UIImage) {
        
        
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.pngData() else { return }
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
            
        }
        
        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }
        
    }
    func getDate() {
        
        DateTextFieldOutlet.text =  defaults.string(forKey: dateUser)
        print("GetName Finish")
        
    }
    //////////////
    var datePicker:UIDatePicker =
    {
        let picker = UIDatePicker()
        
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(datePickerChanged(_sender:)), for: .valueChanged)
        
        picker.minimumDate = Calendar.current.date(byAdding: .year, value: -70, to: Date())
        picker.maximumDate = Calendar.current.date(byAdding: .year, value: -0, to: Date())
        
        picker.locale = Locale(identifier: "ru_RU") as Locale
        picker.timeZone = NSTimeZone.local
        
        return picker
    }()
    
    func createToolBar(){
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title:NSLocalizedString("Done", comment: "DateKeyboardButtn") , style: .plain, target: self, action: #selector(self.dismissKeyboard))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([flexSpace,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        DateTextFieldOutlet.inputAccessoryView = toolBar
        
    }
    func getDateFormaPicker()
    {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd.MM.yyyy"
        DateTextFieldOutlet.text = formatter.string(from: datePicker.date)
        
    }
    
    @objc func dismissKeyboard()
    {
        
        view.endEditing(true)
    }
    func titleTextChangeDate ()
{
    
    titleTextChangeDateOutlet.text = defaults.string(forKey: nameTitle)! + NSLocalizedString(" , now please let me know when is your birthday. This is necessarily for completing your daily personal horoscopes."
, comment: "titleTextChangeDate")    }
    override func viewWillAppear(_ animated: Bool) {
        changeButnDateOutlet.isHidden = true
        titleTextChangeDate()
    }
    func setButnChengeDate()
    {
        changeButnDateOutlet.clipsToBounds = true
        changeButnDateOutlet.layer.cornerRadius = 9.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let localID = Locale.preferredLanguages.first
        datePicker.locale = Locale(identifier: localID!)
        setButnChengeDate()
        createToolBar()
        DateTextFieldOutlet.inputView = datePicker
        dismissKeyboard()
        getDate()
        
        DateTextFieldOutlet.addLine(position: .LINE_POSITION_BOTTOM5, color: .white, width: 1)
        
        
    }
    
    @objc func datePickerChanged(_sender: UIDatePicker){
        getDateFormaPicker()
         changeButnDateOutlet.isHidden = false
        Singleton.shared.dateUser = DateTextFieldOutlet.text!
        ZodiacImage()
        saveImage(imageName: Singleton.shared.nameImageZodiac, image: Singleton.shared.zodiacImage!)
        print("Save iamge change Piker \(Singleton.shared.nameImageZodiac)")
        print("\(Singleton.shared.zodiacUser)")
        saveDate()
        
        print("Save  Changed")
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    override  func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func ZodiacImage()
    {
        let selectedDate = datePicker.date
        let datecomponents = datePicker.calendar.dateComponents([.month, .day], from: selectedDate)
        let day  = datecomponents.day
        let month = datecomponents.month
        
        if  (21 <= day! && month == 3)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Aries", comment: "OvenZodiac")
            
            zodiacImage.image = UIImage(named:"Oven")
            Singleton.shared.zodiacImage = UIImage(named: "Oven")
            Singleton.shared.nameImageZodiac = "Oven.png"
        }   else if  (day! <= 20 && month == 4) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Aries", comment: "OvenZodiac")
            
            zodiacImage.image = UIImage(named:"Oven")
            Singleton.shared.zodiacImage = UIImage(named: "Oven")
            Singleton.shared.nameImageZodiac = "Oven.png"
        }
        ///////////////
        if (21 <= day! && month == 4)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Taurus", comment: "TelecZodiac")
            
            zodiacImage.image = UIImage(named:"Telec")
            Singleton.shared.zodiacImage = UIImage(named: "Telec")
            Singleton.shared.nameImageZodiac = "Telec"
        }else if (20 >= day! && month == 5)
        {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Taurus", comment: "TelecZodiac")
            
            zodiacImage.image = UIImage(named:"Telec")
            Singleton.shared.zodiacImage = UIImage(named: "Telec")
            Singleton.shared.nameImageZodiac = "Telec"
        }
        /////////////
        if (21 <= day! && month == 5)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Gemini", comment: "BleznecyZodiac")
            
            zodiacImage.image = UIImage(named:"Bleznecy")
            Singleton.shared.zodiacImage = UIImage(named: "Bleznecy")
            Singleton.shared.nameImageZodiac = "Bleznecy.png"
        }else if (20 >= day! && month == 6)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Gemini", comment: "BleznecyZodiac")
            
            
            zodiacImage.image = UIImage(named:"Bleznecy")
            Singleton.shared.zodiacImage = UIImage(named: "Bleznecy")
            Singleton.shared.nameImageZodiac = "Bleznecy.png"
        }
        ///////
        
        if (21 <= day! && month == 6)
        {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Cancer", comment: "RackZodiac")
            
            
            zodiacImage.image = UIImage(named:"rak")
            Singleton.shared.zodiacImage = UIImage(named: "rak")
            Singleton.shared.nameImageZodiac = "rak.png"
        }else if (21 >= day! && month == 7)
        {            Singleton.shared.zodiacUser = NSLocalizedString("Cancer", comment: "RackZodiac")
            
            zodiacImage.image = UIImage(named:"rak")
            Singleton.shared.zodiacImage = UIImage(named: "rak")
            Singleton.shared.nameImageZodiac = "rak.png"
        }
        //////////
        
        if (22 <= day! && month == 7)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Leo", comment: "LionZodiac")
            
            
            zodiacImage.image = UIImage(named:"Lev")
            Singleton.shared.zodiacImage = UIImage(named: "Lev")
            Singleton.shared.nameImageZodiac = "Lev.png"
        }else if  (22 >= day! && month == 8) {
            Singleton.shared.zodiacUser = NSLocalizedString("Leo", comment: "LionZodiac")
            zodiacImage.image = UIImage(named:"Lev")
            Singleton.shared.zodiacImage = UIImage(named: "Lev")
            Singleton.shared.nameImageZodiac = "Lev.png"
        }
        
        ////////
        
        if (23 <= day! && month == 8)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Virgo", comment: "DevaZodiac")
            
            zodiacImage.image = UIImage(named:"Deva")
            Singleton.shared.zodiacImage = UIImage(named: "Deva")
            Singleton.shared.nameImageZodiac = "Deva.png"
        } else if ( 22 >= day! && month == 9) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Virgo", comment: "DevaZodiac")
            
            zodiacImage.image = UIImage(named:"Deva")
            Singleton.shared.zodiacImage = UIImage(named: "Deva")
            Singleton.shared.nameImageZodiac = "Deva.png"
        }
        /////////
        if ( 23 <= day! && month == 9)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Libra", comment: "VesyZodiac")
            
            zodiacImage.image = UIImage(named:"Vesy")
            Singleton.shared.zodiacImage = UIImage(named: "Vesy")
            Singleton.shared.nameImageZodiac = "Vesy.png"
        }else if ( 22 >= day! && month == 10) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Libra", comment: "VesyZodiac")
            
            zodiacImage.image = UIImage(named:"Vesy")
            Singleton.shared.zodiacImage = UIImage(named: "Vesy")
            Singleton.shared.nameImageZodiac = "Vesy.png"
        }
        //////////
        if  ( 23 <= day! && month == 10)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Scorpio", comment: "ScorpionZodiac")
            
            
            zodiacImage.image = UIImage(named:"Scorpion")
            Singleton.shared.zodiacImage = UIImage(named: "Scorpion")
            Singleton.shared.nameImageZodiac = "Scorpion.png"
        }else if  (21 >= day! && month == 11) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Scorpio", comment: "ScorpionZodiac")
            
            zodiacImage.image = UIImage(named:"Scorpion")
            Singleton.shared.zodiacImage = UIImage(named: "Scorpion")
            Singleton.shared.nameImageZodiac = "Scorpion.png"
        }
        
        /////////
        if (22 <= day! && month == 11)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Sagittarius", comment: "StrelecZodiac")
            
            zodiacImage.image = UIImage(named:"Strelec")
            Singleton.shared.zodiacImage = UIImage(named: "Strelec")
            Singleton.shared.nameImageZodiac = "Strelec.png"
        }else if (21 >= day! && month == 12) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Sagittarius", comment: "StrelecZodiac")
            
            
            zodiacImage.image = UIImage(named:"Strelec")
            Singleton.shared.zodiacImage = UIImage(named: "Strelec")
            Singleton.shared.nameImageZodiac = "Strelec.png"
        }
        ////////
        
        if  (22 <= day! && month == 12)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Capricorn", comment: "KozerogZodiac")
            
            zodiacImage.image = UIImage(named:"Kozerog")
            Singleton.shared.zodiacImage = UIImage(named: "Kozerog")
            Singleton.shared.nameImageZodiac = "Kozerog.png"
        }else if ( 20 >= day! && month == 1){
            
            Singleton.shared.zodiacUser = NSLocalizedString("Capricorn", comment: "KozerogZodiac")
            
            
            
            zodiacImage.image = UIImage(named:"Kozerog")
            Singleton.shared.zodiacImage = UIImage(named: "Kozerog")
            Singleton.shared.nameImageZodiac = "Kozerog.png"
        }
        ///////
        if ( 21 <= day! && month == 1)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Aquarius", comment: "VodoleiZodiac")
            
            
            zodiacImage.image = UIImage(named:"Vodolei")
            Singleton.shared.zodiacImage = UIImage(named: "Vodolei")
            Singleton.shared.nameImageZodiac = "Vodolei.png"
        }else if  (19 >= day! && month == 2) {
            Singleton.shared.zodiacUser = NSLocalizedString("Aquarius", comment: "VodoleiZodiac")
            
            zodiacImage.image = UIImage(named:"Vodolei")
            Singleton.shared.zodiacImage = UIImage(named: "Vodolei")
            Singleton.shared.nameImageZodiac = "Vodolei.png"
        }
        
        //////
        if (20 <= day! && month == 2)
        {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Pisces", comment: "FishesZodiac")
            
            zodiacImage.image = UIImage(named:"Ryba")
            Singleton.shared.zodiacImage = UIImage(named: "Ryba")
            Singleton.shared.nameImageZodiac = "Ryba"
            
        }else if ( 20 >= day! && month == 3) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Pisces", comment: "FishesZodiac")
            
            zodiacImage.image = UIImage(named:"Ryba")
            Singleton.shared.zodiacImage = UIImage(named: "Ryba")
            Singleton.shared.nameImageZodiac = "Ryba"
            
        }
    }
    
}

