//
//  changeStatusVC.swift
//  Oracles
//
//  Created by Macbook Pro on 03.09.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit

protocol DateStatusDelegate: class {
    func userDidEnteredInformation(status: String)
}


class changeStatusVC: UIViewController {
    
    weak   var delegate:DateStatusDelegate? = nil
    
    
    @IBOutlet weak var titleTextChangeStatusOutlet: UILabel!
    
    @IBOutlet weak var FreheartOutlet: UIButton!
    @IBOutlet weak var  noFreheartOutlet: UIButton!
    @IBOutlet weak var crashHeartOutlet: UIButton!
    @IBOutlet weak var weddingheartOutlet: UIButton!
    @IBOutlet weak var freeLabelOutlet: UILabel!
    
    @IBOutlet weak var changeStatusButnOutlet: UIButton!
    let defaults = UserDefaults.standard
    private let key = "statusUser"
    private let keyName = "nameUser"
    
    func titleTextStatusChangeFunc()
    {
        titleTextChangeStatusOutlet.text = defaults.string(forKey: keyName)! + NSLocalizedString(", do you have a significant other or still looking for your soulmate? Indicate your maritel status, so that I can give you some guidance.", comment: "titleTextChangeStatuts") 
    }
    
    @IBAction  func changeStatus(_ sender:UIButton)
    {
        let information:String  = Singleton.shared.statusUser
        delegate!.userDidEnteredInformation(status: information)
        self.navigationController?.popViewController(animated:true)

        print("ready status")
    }
    @IBAction func statusOneActionButton(_ sender: UIButton) {
        Singleton.shared.statusUser = NSLocalizedString("Single", comment:"Singleton(FreeStatus)")
        
        print("svoboda OK")
        FreheartOutlet.isHidden = true
        noFreheartOutlet.isHidden = false
        crashHeartOutlet.isHidden = false
        weddingheartOutlet.isHidden = false
        changeStatusButnOutlet.isHidden = false

        defaults.set(Singleton.shared.statusUser, forKey: key)
    }
    
    
    @IBAction func statusTwoActionButton(_ sender: UIButton) {
          Singleton.shared.statusUser =  NSLocalizedString("In a relationship ", comment: "Singleton(RelationShip")
        print("zanyat OK")
        FreheartOutlet.isHidden = false
        noFreheartOutlet.isHidden = true
        crashHeartOutlet.isHidden = false
        weddingheartOutlet.isHidden = false
        changeStatusButnOutlet.isHidden = false

        defaults.set(Singleton.shared.statusUser, forKey: key)
    }
    
    @IBAction func statusThreeActionButton(_ sender: UIButton) {
        
        Singleton.shared.statusUser = NSLocalizedString("Married", comment: "Singleton(Wedding)")

        print("brakOK")
        FreheartOutlet.isHidden = false
        noFreheartOutlet.isHidden = false
        crashHeartOutlet.isHidden = false
        weddingheartOutlet.isHidden = true
        changeStatusButnOutlet.isHidden = false

        defaults.set(Singleton.shared.statusUser, forKey: key)
    }
    
    @IBAction func statusFourActionButton(_ sender: UIButton) {
       Singleton.shared.statusUser = NSLocalizedString("It's complicated", comment: "Status(Broken heart)")
        print("slozhno OK")
        FreheartOutlet.isHidden = false
        noFreheartOutlet.isHidden = false
        crashHeartOutlet.isHidden = true
        weddingheartOutlet.isHidden = false
        changeStatusButnOutlet.isHidden = false

        defaults.set(Singleton.shared.statusUser, forKey: key)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
     
        setButnChangeStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
           titleTextStatusChangeFunc()
       changeStatusButnOutlet.isHidden = true
    }
    func setButnChangeStatus()
    {
        changeStatusButnOutlet.clipsToBounds = true
        changeStatusButnOutlet.layer.cornerRadius = 9.0
    }
}

