//
//  StartVC.swift
//  FinishOracles
//
//  Created by Macbook Pro on 06.09.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit

class StartVC: UIViewController, UINavigationControllerDelegate{
    
    @IBOutlet weak var navigatorControllerItemOutlet: UINavigationItem!
    
    @IBOutlet weak var startButnOutlet:UIButton!
    @IBAction func startButtn (_ sender :UIButton!)
    {
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupBut()
    
    }
    func setupBut()
    {
        startButnOutlet.clipsToBounds = true
        startButnOutlet.layer.cornerRadius = 9.0
    }
    
    func setupNavBar()
    {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .white
        
        
    }
}
