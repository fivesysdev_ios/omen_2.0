//
//  changeGenderVC.swift
//  Oracles
//
//  Created by Macbook Pro on 03.09.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit

protocol DateGenderDelegate: class {
    func userDidEnteredInformation(gender: String)
}

class changeGenderVC: UIViewController {
    
    weak var delegate:DateGenderDelegate? = nil
    
    @IBOutlet weak var titleTextChangeGenderOutlet: UILabel!
    
    
    @IBOutlet weak var FemaleOutlet: UIButton!
    
    @IBOutlet weak var MaleOutlet: UIButton!
    
    @IBOutlet weak var changeButtnOutlet: UIButton!
    let defaults = UserDefaults.standard
    private let key = "genderUser"
    private let keyName = "nameUser"
    
    func titleTextChageGenderFunc()
    {
        
        
        titleTextChangeGenderOutlet.text = defaults.string(forKey: keyName)! + NSLocalizedString(", knowing your gender, it will be easier for me to evaluate your personal and business sides of life and also your relationship.", comment: "titleChangeGender") 
    }
    
    @IBAction func changeResult(_ sender: UIButton)
    {
        let information:String  = Singleton.shared.genderUser
        delegate!.userDidEnteredInformation(gender: information)
        self.navigationController?.popViewController(animated:true)

        print("Ready")
    }
    
    @IBAction func changeMaleGenderAction(_ sender: UIButton) {
        
        Singleton.shared.genderUser = NSLocalizedString("Male", comment: "Singleton(Male)")
         changeButtnOutlet.isHidden = false
        print("male ok")
        MaleOutlet.isHidden = true
        FemaleOutlet.isHidden = false
        defaults.set(Singleton.shared.genderUser, forKey: key)
        
        
    }
    
    
    @IBAction func changeFemaleGenderAction(_ sender: UIButton) {
        Singleton.shared.genderUser = NSLocalizedString("Female", comment: "Singleton(Female)")
        print("female ok")
        changeButtnOutlet.isHidden = false

        MaleOutlet.isHidden = false
        FemaleOutlet.isHidden = true
        defaults.set(Singleton.shared.genderUser, forKey: key)
    }
    override func viewWillAppear(_ animated: Bool) {
        changeButtnOutlet.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        titleTextChageGenderFunc()
        setChangeButn()
        
    }
    func setChangeButn()
    {
        changeButtnOutlet.clipsToBounds = true
        changeButtnOutlet.layer.cornerRadius = 9.0
        
    }
}
