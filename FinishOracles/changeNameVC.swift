//
//  changeNameVC.swift
//  Oracles
//
//  Created by Macbook Pro on 03.09.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

//
//  sTwoViewControllerViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 05.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit
import UserNotifications

protocol DataEnteredDelegate: class {
    func userDidEnterInformation(info: String)
}


enum LINE_POSITIONSet {
    case LINE_POSITION_TOPSet
    case LINE_POSITION_BOTTOMSet
}

extension UIView {
    func addLine(position : LINE_POSITIONSet, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOPSet:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOMSet:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}


class changeNameVC: UIViewController,UINavigationBarDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var nameTextFieldOutlet: UITextField!
    
    @IBOutlet weak var changeButtonOutlet: UIButton!
    
    weak var delegate: DataEnteredDelegate? = nil
    
    @IBAction func changeActionButn(_ sender: UIButton) {
        
     
        Singleton.shared.nameUser = nameTextFieldOutlet.text!
        delegate?.userDidEnterInformation(info: nameTextFieldOutlet.text!)
           saveName()
         self.navigationController?.popViewController(animated:true)
        
    }
    
    let defaults = UserDefaults.standard
    
    private let Key = "nameUser"
    
    func saveName()
    {
        
        defaults.set(nameTextFieldOutlet.text, forKey: Key)
        defaults.set(Singleton.shared.nameUser, forKey: Key)
        
        print("save finish")
    }
    
    func getName() {
        nameTextFieldOutlet.text =  defaults.string(forKey: Key)
        
        print("GetName Finish")
        
    }
    func setButnChangeName()
    {
        changeButtonOutlet.clipsToBounds = true
        changeButtonOutlet.layer.cornerRadius = 9.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setButnChangeName()
        nameTextFieldOutlet.addLine(position: .LINE_POSITION_BOTTOMSet, color: .white, width: 1)
        
        nameTextFieldOutlet.delegate = self
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification   ,  object: nil, queue: nil) {(nc) in
            self.view.frame.origin.y = -300
        }
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification  ,  object: nil, queue: nil) {(nc) in
            self.view.frame.origin.y = 0.0
        }
        
        getName()
        
     
        
    }
    override func viewWillAppear(_ animated: Bool) {
        changeButtonOutlet.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         changeButtonOutlet.isHidden = false
        textField.resignFirstResponder()
        return true
    }
    override  func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}

