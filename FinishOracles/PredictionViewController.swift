//
//  PredictionViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 23.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit


class PredictionViewController: UIViewController{
    
    @IBOutlet weak var predictLabel: UILabel!
    @IBOutlet weak var nameAppPredictLabel: UILabel!
    @IBOutlet weak var waitLabel: UILabel!
    
    @IBOutlet weak var toDateOutletLabel: UILabel!
    @IBOutlet weak var zodiacImagePredict: UIImageView!
    
    
    func saveDatePredictFunc()
    {
        let oldDatePredict = Date()
        UserDefaults.standard.set(oldDatePredict, forKey: dateUser)
        
    }
    
    
    
    func showPredictToDay()
    {
        
        guard let olderDate = UserDefaults.standard.object(forKey: dateUser) as? Date,
            Calendar.current.compare(olderDate, to: Date(), toGranularity: .day) != .orderedAscending else {
                print("YES NEWPREDICT")
                readFilePrediction()
                saveDatePredictFunc()
                savePredict()
                return
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        showDate()
        print(NSLocale.preferredLanguages[0])
        
    }
    func showDate()
    {
        let dateInLabel = UserDefaults.standard.object(forKey: dateUser) as! Date
        let DateForm = DateFormatter()
        DateForm.dateFormat = "dd/MM/yyyy"
        toDateOutletLabel.text = DateForm.string(from: dateInLabel)
    }
    func readFilePrediction(){
        let preferredLanguage = NSLocale.preferredLanguages[0]
        
        if  preferredLanguage == "ru" {
            
            let path = Bundle.main.path(forResource: "predictRUS", ofType: "txt")
            
            let url = URL(fileURLWithPath: path!)
            
            let contentString = try! NSString(contentsOf: url, encoding: String.Encoding.utf8.rawValue)
            
            let  msgString = contentString.components(separatedBy: [".","!"]).randomElement()!.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\n", with: "")
            
            self.predictLabel.text = msgString
            
        }else {
            let path = Bundle.main.path(forResource: "predictENG", ofType: "txt")
            
            let url = URL(fileURLWithPath: path!)
            
            let contentString = try! NSString(contentsOf: url, encoding: String.Encoding.utf8.rawValue)
            
            let  msgString = contentString.components(separatedBy: [".","!"]).randomElement()!.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\n", with: "")
            
            self.predictLabel.text = msgString
        }
    }
    let defaults = UserDefaults.standard
    private let predictUser = "predictUser"
    private let dateUser = "datePredict"
    private let zodiacImageNameGet = "zodiacImageName"
    
    func savePredict()
    {
        defaults.set(predictLabel.text, forKey: predictUser)
        
        print("save predict")
    }
    
    func getPredict()
    {
        zodiacImagePredict.image = Singleton.shared.zodiacImage
        
        predictLabel.text = defaults.string(forKey: predictUser)
        toDateOutletLabel.text = defaults.string(forKey: dateUser)
        
        
        let zodiacNameGet = defaults.string(forKey:zodiacImageNameGet)!
        zodiacImagePredict.image =  loadImageFromDiskWith(fileName: zodiacNameGet)
        print("get predict")
    }
    
    // LoadImage
    func loadImageFromDiskWith(fileName: String) -> UIImage? {
        
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            print("Load Image func \(Singleton.shared.nameImageZodiac)")
            return image
            
        }
        
        return nil
    }
    
    
    
    @IBAction func shareAction(_ sender: Any) {
        
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      
        var imagesToShare = [AnyObject]()
        imagesToShare.append(image!)
        
        let activityViewController = UIActivityViewController(activityItems: imagesToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        present(activityViewController, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showPredictToDay()
        predictLabel.clipsToBounds = true
        predictLabel.layer.cornerRadius = 9.0
        getPredict()
        
        
    }
}




