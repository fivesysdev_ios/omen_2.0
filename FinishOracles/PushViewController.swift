//
//  PushViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 24.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit
import UserNotifications

class PushViewController: UIViewController,UNUserNotificationCenterDelegate {

    @IBOutlet weak var PushButtonOutlet:UIButton!
    let defaults = UserDefaults.standard
    private let switchKey = "switchUser"
    @IBAction func PushButtonAction(_ sender: UIButton) {
        PushViewController.push()
        Singleton.shared.switchBool = true
        defaults.set(Singleton.shared.switchBool, forKey: switchKey)
        print("push on" )
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPushButn()
    }
  class  func push()
    {
        
        let content = UNMutableNotificationContent()
        content.title = NSLocalizedString("The horoscope for today is ready!!", comment: "content.Title.Push")
        content.body =
       NSLocalizedString("Find out  what fate has the future prepared for you today!", comment: "content.Body.Push")
        content.sound = UNNotificationSound.default
        
        let date = Date()
        var triggerDate = Calendar.current.dateComponents([.hour,.minute,.second,], from: date)
        triggerDate.hour = 9
          triggerDate.minute = 0
          triggerDate.second = 0
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: true)
        let request = UNNotificationRequest(identifier: "Oneindifer", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil )
        
    }
  
    func setupPushButn()
    {
        PushButtonOutlet.clipsToBounds = true
        PushButtonOutlet.layer.cornerRadius = 9.0
        
    }
}
