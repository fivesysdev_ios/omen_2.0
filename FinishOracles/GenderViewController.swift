//
//  GenderViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 20.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit

class  GenderViewController: UIViewController {

    
    @IBOutlet weak var FemaleOutlet: UIButton!
    @IBOutlet weak var MaleOutlet: UIButton!

    @IBOutlet weak var contGenderButnOutlet: UIButton!
    @IBOutlet weak var titleTextGenderOutlet: UILabel!
    
  let defaults = UserDefaults.standard
    private let key = "genderUser"
    
    
    @IBAction func MaleActionButton(_ sender: UIButton) {
        
        Singleton.shared.genderUser = NSLocalizedString("Male", comment: "Singleton(Male)")
        MaleOutlet.isHidden = true
        FemaleOutlet.isHidden = false
        contGenderButnOutlet.isHidden = false
        defaults.set(Singleton.shared.genderUser, forKey: key)
    }
    
    
    @IBAction func FemaleActionButton(_ sender: UIButton) {
        Singleton.shared.genderUser = NSLocalizedString("Female", comment: "Singleton(Female)")
        MaleOutlet.isHidden = false
        FemaleOutlet.isHidden = true
        contGenderButnOutlet.isHidden = false
        defaults.set(Singleton.shared.genderUser, forKey: key)
    }
    override func viewWillAppear(_ animated: Bool) {
        contGenderButnOutlet.isHidden = true
    }
  
 
    func titleGenderText()
    {
        titleTextGenderOutlet.text = Singleton.shared.nameUser + NSLocalizedString(",  knowing your gender, it will be easier for me to evaluate your personal and business sides of life and also your relationship.", comment: "titleTextGender")
        
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        titleGenderText()
       setupButn()

        
    }
  func setupButn()
  {contGenderButnOutlet.clipsToBounds = true
    contGenderButnOutlet.layer.cornerRadius = 9.0
    
    }


}
