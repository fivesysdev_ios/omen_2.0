//
//  DateViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 23.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit

//////

enum LINE_POSITION3 {
    case LINE_POSITION_TOP3
    case LINE_POSITION_BOTTOM3
}

extension UIView {
    func addLine(position : LINE_POSITION3, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP3:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM3:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}

/// CLASS


class DateViewController: UIViewController ,UITextFieldDelegate,UIPickerViewDelegate{
    
    
    
    @IBOutlet weak var zodiacImage: UIImageView!
    
    @IBOutlet weak var DateTextFieldOutlet: UITextField!
    
    @IBOutlet weak var titleTextDateOutlet: UILabel!
    
    @IBOutlet weak var contButtnDateOutlet: UIButton!
    let defaults = UserDefaults.standard
    
    private let dateUser = "dateUser"
    private let zodiacNameUser = "zodiacUser"
    private let zodiacImageUser = "zodiacImageName"
    
    
    func saveDate()
    {
        defaults.set(DateTextFieldOutlet.text, forKey: dateUser)
        defaults.set(Singleton.shared.dateUser, forKey: dateUser)
        defaults.set(Singleton.shared.zodiacUser, forKey: zodiacNameUser)
        defaults.set(Singleton.shared.nameImageZodiac, forKey: zodiacImageUser)
        
        
        print("save finish")
    }
    
    func titleDateText()
    {
        titleTextDateOutlet.text = Singleton.shared.nameUser + NSLocalizedString(", now please let me know when is your birthday. This is necessarily for completing your daily personal horoscopes.", comment: "titleTextDate")
    }
    func saveImage(imageName: String, image: UIImage) {
        
        
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.pngData() else { return }
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
            
        }
        
        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }
        
    }
    func getDate() {
        
        DateTextFieldOutlet.text =  defaults.string(forKey: dateUser)
        print("Getdate Finish")
        
    }
    
    /////////////
    
    var datePicker:UIDatePicker =
    {
        
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.calendar = Calendar.current
        picker.addTarget(self, action: #selector(datePickerChanged(_sender:)), for: .valueChanged)
        
        picker.minimumDate = Calendar.current.date(byAdding: .year, value: -70, to: Date())
        picker.maximumDate = Calendar.current.date(byAdding: .year, value: -0, to: Date())
        
        
        picker.locale = Locale(identifier: "ru_RU") as Locale
        picker.timeZone = NSTimeZone.local
        
        
        
        return picker
    }()
    
    func createToolBar(){
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title:NSLocalizedString("Done", comment: "KeyboardDateButn") , style: .plain, target: self, action: #selector(self.dismissKeyboard))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        
        toolBar.setItems([flexSpace,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        DateTextFieldOutlet.inputAccessoryView = toolBar
        
    }
    
    func getDateFormaPicker()
    {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd.MM.yyyy"
        DateTextFieldOutlet.text = formatter.string(from: datePicker.date)
        
    }
    
    @objc func dismissKeyboard()
    {
        
        view.endEditing(true)
        
        
    }
    
    func setupButnCont()
    {
        contButtnDateOutlet.clipsToBounds = true
        contButtnDateOutlet.layer.cornerRadius = 9.0
    }
    override func viewWillAppear(_ animated: Bool) {
        contButtnDateOutlet.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleDateText()
        let localID = Locale.preferredLanguages.first
        datePicker.locale = Locale(identifier: localID!)
        createToolBar()
        DateTextFieldOutlet.inputView = datePicker
        dismissKeyboard()
        getDate()
        DateTextFieldOutlet.addLine(position: .LINE_POSITION_BOTTOM3, color: .white, width: 1)
        setupButnCont()
    }
    
    @objc func datePickerChanged(_sender: UIDatePicker){
        getDateFormaPicker()
        contButtnDateOutlet.isHidden = false
        
        Singleton.shared.dateUser = DateTextFieldOutlet.text!
        ZodiacImage()
        saveImage(imageName: Singleton.shared.nameImageZodiac, image: Singleton.shared.zodiacImage!)
        saveDate()
        print("Save  Changed")
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    override  func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    let preferredLanguage = NSLocale.preferredLanguages[0]
    
    
    func ZodiacImage()
    {
        let selectedDate = datePicker.date
        let datecomponents = datePicker.calendar.dateComponents([.month, .day], from: selectedDate)
        let day  = datecomponents.day
        let month = datecomponents.month
        
        if  (21 <= day! && month == 3)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Aries", comment: "OvenZodiac")
            
            zodiacImage.image = UIImage(named:"Oven")
            Singleton.shared.zodiacImage = UIImage(named: "Oven")
            Singleton.shared.nameImageZodiac = "Oven.png"
        }   else if  (day! <= 20 && month == 4) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Aries", comment: "OvenZodiac")
            
            zodiacImage.image = UIImage(named:"Oven")
            Singleton.shared.zodiacImage = UIImage(named: "Oven")
            Singleton.shared.nameImageZodiac = "Oven.png"
        }
        ///////////////
        if (21 <= day! && month == 4)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Taurus", comment: "TelecZodiac")
            
            zodiacImage.image = UIImage(named:"Telec")
            Singleton.shared.zodiacImage = UIImage(named: "Telec")
            Singleton.shared.nameImageZodiac = "Telec"
        }else if (20 >= day! && month == 5)
        {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Taurus", comment: "TelecZodiac")
            
            zodiacImage.image = UIImage(named:"Telec")
            Singleton.shared.zodiacImage = UIImage(named: "Telec")
            Singleton.shared.nameImageZodiac = "Telec"
        }
        /////////////
        if (21 <= day! && month == 5)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Gemini", comment: "BleznecyZodiac")
            
            zodiacImage.image = UIImage(named:"Bleznecy")
            Singleton.shared.zodiacImage = UIImage(named: "Bleznecy")
            Singleton.shared.nameImageZodiac = "Bleznecy.png"
        }else if (20 >= day! && month == 6)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Gemini", comment: "BleznecyZodiac")
            
            
            zodiacImage.image = UIImage(named:"Bleznecy")
            Singleton.shared.zodiacImage = UIImage(named: "Bleznecy")
            Singleton.shared.nameImageZodiac = "Bleznecy.png"
        }
        ///////
        
        if (21 <= day! && month == 6)
        {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Cancer", comment: "RackZodiac")
            
            
            zodiacImage.image = UIImage(named:"rak")
            Singleton.shared.zodiacImage = UIImage(named: "rak")
            Singleton.shared.nameImageZodiac = "rak.png"
        }else if (21 >= day! && month == 7)
        {            Singleton.shared.zodiacUser = NSLocalizedString("Cancer", comment: "RackZodiac")
            
            zodiacImage.image = UIImage(named:"rak")
            Singleton.shared.zodiacImage = UIImage(named: "rak")
            Singleton.shared.nameImageZodiac = "rak.png"
        }
        //////////
        
        if (22 <= day! && month == 7)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Leo", comment: "LionZodiac")
            
            
            zodiacImage.image = UIImage(named:"Lev")
            Singleton.shared.zodiacImage = UIImage(named: "Lev")
            Singleton.shared.nameImageZodiac = "Lev.png"
        }else if  (22 >= day! && month == 8) {
            Singleton.shared.zodiacUser = NSLocalizedString("Leo", comment: "LionZodiac")
            zodiacImage.image = UIImage(named:"Lev")
            Singleton.shared.zodiacImage = UIImage(named: "Lev")
            Singleton.shared.nameImageZodiac = "Lev.png"
        }
        
        ////////
        
        if (23 <= day! && month == 8)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Virgo", comment: "DevaZodiac")
            
            zodiacImage.image = UIImage(named:"Deva")
            Singleton.shared.zodiacImage = UIImage(named: "Deva")
            Singleton.shared.nameImageZodiac = "Deva.png"
        } else if ( 22 >= day! && month == 9) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Virgo", comment: "DevaZodiac")
            
            zodiacImage.image = UIImage(named:"Deva")
            Singleton.shared.zodiacImage = UIImage(named: "Deva")
            Singleton.shared.nameImageZodiac = "Deva.png"
        }
        /////////
        if ( 23 <= day! && month == 9)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Libra", comment: "VesyZodiac")
            
            zodiacImage.image = UIImage(named:"Vesy")
            Singleton.shared.zodiacImage = UIImage(named: "Vesy")
            Singleton.shared.nameImageZodiac = "Vesy.png"
        }else if ( 22 >= day! && month == 10) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Libra", comment: "VesyZodiac")
            
            zodiacImage.image = UIImage(named:"Vesy")
            Singleton.shared.zodiacImage = UIImage(named: "Vesy")
            Singleton.shared.nameImageZodiac = "Vesy.png"
        }
        //////////
        if  ( 23 <= day! && month == 10)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Scorpio", comment: "ScorpionZodiac")
            
            
            zodiacImage.image = UIImage(named:"Scorpion")
            Singleton.shared.zodiacImage = UIImage(named: "Scorpion")
            Singleton.shared.nameImageZodiac = "Scorpion.png"
        }else if  (21 >= day! && month == 11) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Scorpio", comment: "ScorpionZodiac")
            
            zodiacImage.image = UIImage(named:"Scorpion")
            Singleton.shared.zodiacImage = UIImage(named: "Scorpion")
            Singleton.shared.nameImageZodiac = "Scorpion.png"
        }
        
        /////////
        if (22 <= day! && month == 11)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Sagittarius", comment: "StrelecZodiac")
            
            zodiacImage.image = UIImage(named:"Strelec")
            Singleton.shared.zodiacImage = UIImage(named: "Strelec")
            Singleton.shared.nameImageZodiac = "Strelec.png"
        }else if (21 >= day! && month == 12) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Sagittarius", comment: "StrelecZodiac")
            
            
            zodiacImage.image = UIImage(named:"Strelec")
            Singleton.shared.zodiacImage = UIImage(named: "Strelec")
            Singleton.shared.nameImageZodiac = "Strelec.png"
        }
        ////////
        
        if  (22 <= day! && month == 12)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Capricorn", comment: "KozerogZodiac")
            
            zodiacImage.image = UIImage(named:"Kozerog")
            Singleton.shared.zodiacImage = UIImage(named: "Kozerog")
            Singleton.shared.nameImageZodiac = "Kozerog.png"
        }else if ( 20 >= day! && month == 1){
            
            Singleton.shared.zodiacUser = NSLocalizedString("Capricorn", comment: "KozerogZodiac")
            
            
            
            zodiacImage.image = UIImage(named:"Kozerog")
            Singleton.shared.zodiacImage = UIImage(named: "Kozerog")
            Singleton.shared.nameImageZodiac = "Kozerog.png"
        }
        ///////
        if ( 21 <= day! && month == 1)
        {
            Singleton.shared.zodiacUser = NSLocalizedString("Aquarius", comment: "VodoleiZodiac")
            
            
            zodiacImage.image = UIImage(named:"Vodolei")
            Singleton.shared.zodiacImage = UIImage(named: "Vodolei")
            Singleton.shared.nameImageZodiac = "Vodolei.png"
        }else if  (19 >= day! && month == 2) {
            Singleton.shared.zodiacUser = NSLocalizedString("Aquarius", comment: "VodoleiZodiac")
            
            zodiacImage.image = UIImage(named:"Vodolei")
            Singleton.shared.zodiacImage = UIImage(named: "Vodolei")
            Singleton.shared.nameImageZodiac = "Vodolei.png"
        }
        
        //////
        if (20 <= day! && month == 2)
        {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Pisces", comment: "FishesZodiac")
            
            zodiacImage.image = UIImage(named:"Ryba")
            Singleton.shared.zodiacImage = UIImage(named: "Ryba")
            Singleton.shared.nameImageZodiac = "Ryba"
            
        }else if ( 20 >= day! && month == 3) {
            
            Singleton.shared.zodiacUser = NSLocalizedString("Pisces", comment: "FishesZodiac")
            
            zodiacImage.image = UIImage(named:"Ryba")
            Singleton.shared.zodiacImage = UIImage(named: "Ryba")
            Singleton.shared.nameImageZodiac = "Ryba"
            
        }
        
        
    }
    
}


