//
//  TimeBDayViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 21.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit
import UserNotifications
enum LINE_POSITION2 {
    case LINE_POSITION_TOP2
    case LINE_POSITION_BOTTOM2
}

extension UIView {
    func addLine(position : LINE_POSITION2, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP2:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM2:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}



class TimeBDayViewController: UIViewController, UINavigationBarDelegate, UITextFieldDelegate, UIPickerViewDelegate {
    
    
    
    @IBOutlet weak var titlteTimeTextOutlet: UILabel!
    
    @IBOutlet weak var contButnTimeOutlet: UIButton!
    @IBOutlet weak var timeTextFieldOutlet: UITextField!
    
    
    @IBAction func forgotTimeActionButton(_ sender: UIButton) {
        Singleton.shared.timeUser = "----"
        defaults.set(Singleton.shared.timeUser, forKey: Key)
    }
    
    let defaults = UserDefaults.standard
    private let Key = "timeUser"
    private let KeyName = "nameUser"
    
    func saveTime()
    {
        
        defaults.set(timeTextFieldOutlet.text, forKey: Key)
        defaults.set(Singleton.shared.timeUser, forKey: Key)
        print("save finish")
    }
    
    func getTime() {
        timeTextFieldOutlet.text =  defaults.string(forKey:Key)
        
        print("GetName Finish")
        
    }
    override func viewWillAppear(_ animated: Bool) {
        contButnTimeOutlet.isHidden = true
        titleTimeText()
    }
    func setupContTibeButn()
    {
        contButnTimeOutlet.clipsToBounds = true
        contButnTimeOutlet.layer.cornerRadius = 9.0
    }
    func titleTimeText()
    {
        titlteTimeTextOutlet.text = defaults.string(forKey: KeyName)! + NSLocalizedString(", do you know the time you were born at? - this will help me to compose an astrological report more accurately ", comment: "titleTextTimeBDay")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timeTextFieldOutlet.addLine(position: .LINE_POSITION_BOTTOM2, color: .white, width: 1)
        setupContTibeButn()
        timeTextFieldOutlet.delegate = self
        
        
        getTime()
        timeTextFieldOutlet.inputView = datePicker
        
        createToolBar()
        let localID = Locale.preferredLanguages.first
        datePicker.locale = Locale(identifier: localID!)
        dismissKeyboard()
        
        
    }
    var datePicker:UIDatePicker =
    {
        
        let picker = UIDatePicker()
        
        picker.datePickerMode = .time
        picker.addTarget(self, action: #selector(datePickerChanged(_sender:)), for: .valueChanged)
        
        
        picker.locale = Locale(identifier: "ru_RU") as Locale
        picker.timeZone = NSTimeZone.local
        
        return picker
    }()
    
    func createToolBar(){
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.dismissKeyboard))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        
        toolBar.setItems([flexSpace,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        timeTextFieldOutlet.inputAccessoryView = toolBar
        
        
        
    }
    
    @objc func datePickerChanged(_sender: UIDatePicker){
        getDateFormaPicker()
        contButnTimeOutlet.isHidden = false
        
        Singleton.shared.timeUser = timeTextFieldOutlet.text!
        saveTime()
        print("Save  Changed")
        
    }
    
    func getDateFormaPicker()
    {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "HH:mm"
        timeTextFieldOutlet.text = formatter.string(from: datePicker.date)
    }
    @objc func dismissKeyboard()
    {
        getDateFormaPicker()
        view.endEditing(true)
        
        
    }
    
    override  func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        saveTime()
        textField.resignFirstResponder()
        return true
    }
    
}


