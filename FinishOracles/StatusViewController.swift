//
//  StatusViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 20.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit


class StatusViewController: UIViewController{
    
    
    @IBOutlet weak var FreheartOutlet: UIButton!
    @IBOutlet weak var noFreheartOutlet: UIButton!
    @IBOutlet weak var crashHeartOutlet: UIButton!
    @IBOutlet weak var weddingheartOutlet: UIButton!
    
    @IBOutlet weak var titleTextStatusOutlet: UILabel!
    @IBOutlet weak var contButnStatusOutlet: UIButton!
    
    @IBOutlet weak var freLabelOutlet: UILabel!
    let defaults = UserDefaults.standard
    private let key = "statusUser"
    
    
    @IBAction func statusOneActionButton(_ sender: UIButton) {
        
            Singleton.shared.statusUser = NSLocalizedString("Single", comment:"Singleton(FreeStatus)")
        
        FreheartOutlet.isHidden = true
        noFreheartOutlet.isHidden = false
        crashHeartOutlet.isHidden = false
        weddingheartOutlet.isHidden = false
        contButnStatusOutlet.isHidden = false
        
        defaults.set(Singleton.shared.statusUser, forKey: key)
    }
    
    @IBAction func statusTwoActionButton(_ sender: UIButton) {
        
     
            Singleton.shared.statusUser =  NSLocalizedString("In a relationship ", comment: "Singleton(RelationShip")
        
        FreheartOutlet.isHidden = false
        noFreheartOutlet.isHidden = true
        crashHeartOutlet.isHidden = false
        weddingheartOutlet.isHidden = false
        contButnStatusOutlet.isHidden = false
        
        defaults.set(Singleton.shared.statusUser, forKey: key)
    }
    
    @IBAction func statusThreeActionButton(_ sender: UIButton) {
         Singleton.shared.statusUser = NSLocalizedString("Married", comment: "Singleton(Wedding)")
      
        FreheartOutlet.isHidden = false
        noFreheartOutlet.isHidden = false
        crashHeartOutlet.isHidden = false
        weddingheartOutlet.isHidden = true
        contButnStatusOutlet.isHidden = false
        
        defaults.set(Singleton.shared.statusUser, forKey: key)
    }
    
    @IBAction func statusFourActionButton(_ sender: UIButton) {
        
        Singleton.shared.statusUser = NSLocalizedString("It's complicated", comment: "Status(Broken heart)")
        FreheartOutlet.isHidden = false
        noFreheartOutlet.isHidden = false
        crashHeartOutlet.isHidden = true
        weddingheartOutlet.isHidden = false
        contButnStatusOutlet.isHidden = false
        defaults.set(Singleton.shared.statusUser, forKey: key)
    }
    override func viewWillAppear(_ animated: Bool) {
        contButnStatusOutlet.isHidden = true
       
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        titleTextStatus()
        setupContButn()
    }
    func titleTextStatus()
    {
        titleTextStatusOutlet.text = Singleton.shared.nameUser + NSLocalizedString(", do you have a significant other or still looking for your soulmate? Indicate your maritel status, so that I can give you some guidance.", comment: "TitleTextStatus")
    }
    func setupContButn()
    {
        contButnStatusOutlet.clipsToBounds = true
        contButnStatusOutlet.layer.cornerRadius = 9.0
    }
}
