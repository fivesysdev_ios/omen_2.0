//
//  AppDelegate.swift
//  FinishOracles
//
//  Created by Macbook Pro on 06.09.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func  userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        completionHandler([.alert, .sound])
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound])
        {
            (granted,error ) in
            print("granted\(granted)")
        }
        
        
       self.window = UIWindow(frame: UIScreen.main.bounds)
let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
      let def = UserDefaults.standard
     let homeController: UIViewController
     
       let is_authenticated = def.bool(forKey: "is_authenticated") // return false if not found or stored value
     
       if is_authenticated {
        //user logged in
          homeController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarControllerid") as! UITabBarController // create instance of HomeController or from Storyboard
        
       }else {
           homeController = mainStoryboard.instantiateViewController(withIdentifier: "NavigationControllerid") as! UINavigationController
       }
       saveLoggedState()
     
       window?.rootViewController = homeController // change rootViewController to HomeController
       window?.makeKeyAndVisible() // show window
     
        return true
    }
    
    func saveLoggedState() {
        
        let def = UserDefaults.standard
        def.set(true, forKey: "is_authenticated") // save true flag to UserDefaults
        def.synchronize()
        
    }
    
    
}

