//
//  QuestionViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 23.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
    
    @IBOutlet weak var AnswerLabelOutlet: UILabel!
    @IBOutlet weak var titleLabelOutlet: UILabel!
    @IBOutlet weak var backGroundImage: UIImageView!
    @IBOutlet weak var shakeTextOutletlabel: UILabel!
    @IBOutlet weak var startButtnOutlet: UIButton!
    @IBAction func startButtnAtion(_ sender: UIButton) {
     animateImage()
    }
    override  func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        
        if motion == .motionShake
        {
            animateImage()
        }
    
  
        
    }
    var isAnimating: Bool = false //1
    func animateImage(){
        titleLabelOutlet.isHidden = true
        shakeTextOutletlabel.isHidden = true
        startButtnOutlet.setTitle("", for: .normal)
        guard !isAnimating else { //2
            return
        }
        isAnimating = true //3
        CATransaction.begin()
        let rotationAnimation = CABasicAnimation(keyPath:
            "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = Double.pi * 4
        rotationAnimation.duration = 2.0
        CATransaction.setCompletionBlock {
            self.answeForQuest()
        }
        backGroundImage.layer.add(rotationAnimation, forKey: nil)
        CATransaction.commit()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        AnswerLabelOutlet.text = "Press here"
        titleLabelOutlet.isHidden = false
        shakeTextOutletlabel.isHidden = false
    }
    
    func answeForQuest()
    {
      isAnimating = false
        let preferredLanguage = NSLocale.preferredLanguages[0]
        if  preferredLanguage == "ru" {
            let AnswerArrayRUS = [
                "Да",
                "Нет",
                "Без сомнения",
                "Может быть",
                "Забудь об этом!",
                "Хорошие перспективы",
                "Не делай этого",
                "Это возможно ",
                "Ни в коем случае ",
                "Отличная идея! ",
                "Даже не думай ",
                "Определенно - ДА ",
                "Это неплохо",
                "Ну конечно! ",
                " Хватит об этом думать!",
                "Бесспорно",
                " По миои данным - 'Нет'",
                " Перспективы не очень хорошие"]
            
            let randomItem = Int(arc4random() % UInt32(AnswerArrayRUS.count))
            UIView.transition(with: AnswerLabelOutlet, duration: 1, options: .transitionCrossDissolve, animations: {[weak self] in self?.AnswerLabelOutlet.text = "\(AnswerArrayRUS[randomItem])" }, completion: nil)
        }else {
            let AnswerArrayENG = [  "Yes",
                                    "No",
                                    "Without a doubt",
                                    "Maybe",
                                    "Forget it!",
                                    "Good prospects",
                                    "Do not do this",
                                    "It is possible",
                                    "In no case",
                                    "Great idea!",
                                    "Do not even think",
                                    "Definitely yes",
                                    "It's not bad",
                                    "Well, of course!",
                                    "Stop thinking about it!",
                                    "Undoubtedly",
                                    "According to my data - 'No'",
                                    "Prospects are not very good"]
            let randomItem = Int(arc4random() % UInt32(AnswerArrayENG.count))
            UIView.transition(with: AnswerLabelOutlet, duration: 1, options: .transitionCrossDissolve, animations: {[weak self] in self?.AnswerLabelOutlet.text = "\(AnswerArrayENG[randomItem])" }, completion: nil)
        }
        startButtnOutlet.isEnabled = true

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}

