//
//  changeTimeVC.swift
//  Oracles
//
//  Created by Macbook Pro on 03.09.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//


import UIKit
import UserNotifications


protocol DateTimeDelegate: class {
    func userDidEnteredInformation(time: String)
}



enum LINE_POSITION4 {
    case LINE_POSITION_TOP4
    case LINE_POSITION_BOTTOM4
}

extension UIView {
    func addLine(position : LINE_POSITION4, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP4:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM4:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}



class changeTimeVC: UIViewController, UINavigationBarDelegate, UITextFieldDelegate, UIPickerViewDelegate {
    
     weak   var delegate:DateTimeDelegate? = nil
   
    
    
    @IBOutlet weak var changeTimeButnOutlet: UIButton!
    @IBOutlet weak var titleChangeTimeOutletLabel: UILabel!
    @IBOutlet weak var timeTextFieldOutlet: UITextField!
    
    let defaults = UserDefaults.standard
    private let Key = "timeUser"
    private let KeyName = "nameUser"
    
    @IBAction func forgotTimeActionButton(_ sender: UIButton) {
        Singleton.shared.timeUser = "----"
        let information:String  =  Singleton.shared.timeUser
        delegate!.userDidEnteredInformation(time: information)
         defaults.set(Singleton.shared.timeUser, forKey: Key)
        self.navigationController?.popViewController(animated:true)

        print("Ready")
    }
    
    @IBAction func changeTimeAction(_ sender: UIButton!)
    {
        let information:String  =  timeTextFieldOutlet.text!
        delegate!.userDidEnteredInformation(time: information)
        self.navigationController?.popViewController(animated:true)

        print("Ready")
    }
    
  func titleTextChangeTimeFunc()
  {
    titleChangeTimeOutletLabel.text = defaults.string(forKey: KeyName)! + NSLocalizedString(", do you know the time you were born at? - this will help me to compose an astrological report more accurately.", comment: "titleTextChangeTime")
    }
    override func viewWillAppear(_ animated: Bool) {
        titleTextChangeTimeFunc()
        changeTimeButnOutlet.isHidden = true

    }
    func changeTimeButnOutletFunc()
    {
        changeTimeButnOutlet.clipsToBounds = true
        changeTimeButnOutlet.layer.cornerRadius = 9.0
    }
    func saveTime()
    {
        
        defaults.set(timeTextFieldOutlet.text, forKey: Key)
        
        print("save finish")
    }
    
    func getTime() {
        timeTextFieldOutlet.text =  defaults.string(forKey:Key)
        
        print("GetName Finish")
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeTimeButnOutletFunc()
        
        
        timeTextFieldOutlet.addLine(position: .LINE_POSITION_BOTTOM2, color: .white, width: 1)
        
        timeTextFieldOutlet.delegate = self
      
        getTime()
        timeTextFieldOutlet.inputView = datePicker
        
        createToolBar()
        let localID = Locale.preferredLanguages.first
        datePicker.locale = Locale(identifier: localID!)
        dismissKeyboard()
        
        
    }
    var datePicker:UIDatePicker =
    {
        
        let picker = UIDatePicker()
        
        picker.datePickerMode = .time
        picker.addTarget(self, action: #selector(datePickerChanged(_sender:)), for: .valueChanged)        
        picker.locale = Locale(identifier: "ru_RU") as Locale
        picker.timeZone = NSTimeZone.local
        
        return picker
    }()
    
    func createToolBar(){
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "TimeKeyboardButn"), style: .plain, target: self, action: #selector(self.dismissKeyboard))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        
        toolBar.setItems([flexSpace,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        timeTextFieldOutlet.inputAccessoryView = toolBar
        
        
        
    }
    @objc func datePickerChanged(_sender: UIDatePicker){
        getDateFormaPicker()
        saveTime()
        changeTimeButnOutlet.isHidden = false
        Singleton.shared.timeUser = timeTextFieldOutlet.text!
         defaults.set(Singleton.shared.timeUser, forKey: Key)
        print("Save  Changed")
        
    }
    
    func getDateFormaPicker()
    {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "HH:mm"
        timeTextFieldOutlet.text = formatter.string(from: datePicker.date)
    }
    
    
    
    
    @objc func dismissKeyboard()
    {
        getDateFormaPicker()
        view.endEditing(true)
        
        
    }
    
    override  func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        saveTime()
        
        textField.resignFirstResponder()
        return true
    }
    
}


