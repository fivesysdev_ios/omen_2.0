//
//  sTwoViewControllerViewController.swift
//  Oracles
//
//  Created by Macbook Pro on 05.08.2019.
//  Copyright © 2019 GlebSerediuk. All rights reserved.
//

import UIKit
import UserNotifications
enum LINE_POSITION {
    case LINE_POSITION_TOP
    case LINE_POSITION_BOTTOM
}

extension UIView {
    func addLine(position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false 
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}


class sTwoViewControllerViewController: UIViewController,UINavigationBarDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var nameTextFieldOutlet: UITextField!
    
    @IBOutlet weak var contButtonOutlet: UIButton!
    
    let defaults = UserDefaults.standard
    private let Key = "nameUser"
    
    func saveName()
    {
        
        defaults.set(nameTextFieldOutlet.text, forKey: Key)
        
        print("save finish")
    }
    
    func getName() {
        nameTextFieldOutlet.text =  defaults.string(forKey:Key)
        
        print("GetName Finish")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        contButtonOutlet.isHidden = true
       
    }
 func buttnHide()
 {
    if nameTextFieldOutlet.text != "" || nameTextFieldOutlet.text != nil{
        contButtonOutlet.isHidden = false
        
        
    }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextFieldOutlet.addLine(position: .LINE_POSITION_BOTTOM, color: .white, width: 1)
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification   ,  object: nil, queue: nil) {(nc) in
            self.view.frame.origin.y = -300
        }
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification  ,  object: nil, queue: nil) {(nc) in
            self.view.frame.origin.y = 0.0
        }
        nameTextFieldOutlet.delegate = self
        getName()
       
        contButtonOutlet.clipsToBounds = true
        contButtonOutlet.layer.cornerRadius = 9.0
        
    }
   
  
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        Singleton.shared.nameUser = nameTextFieldOutlet.text!
        saveName()
        if self.nameTextFieldOutlet.text != "" || self.nameTextFieldOutlet.text != nil{
              buttnHide()
        }
        textField.resignFirstResponder()
        return true
    }
    override  func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
       
       
    }
    
}

